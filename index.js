const express = require('express');
const shell = require('shelljs');
const app = express();
const port = 9000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/hooks', (req, res) => {
  try {
    shell.echo('start deploy');
    shell.exec('bash deploy.sh', (code, stdout, stderr) => {
      console.log('code:', code);
      stdout && console.log('deploy output:', stdout);
      stderr && console.log('deploy err:', stderr);
    });
    res.send('deploy api');
  } catch (error) {
    console.error(error);
  }
});

app.post('/hooks', (req, res) => {
  try {
    console.log('================>>>: req.body ::: ', req.body);
    shell.echo('start deploy');
    shell.exec('bash deploy.sh', (code, stdout, stderr) => {
      console.log('code:', code);
      stdout && console.log('deploy output:', stdout);
      stderr && console.log('deploy err:', stderr);
    });
    res.send('deploy api');
  } catch (error) {
    console.error(error);
  }
});

app.listen(port, () => {
  console.log(`Server deploy API running port: ${port}`);
});
